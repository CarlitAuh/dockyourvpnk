package com.example.dyv.dockyourvpnk.controller

import android.content.Intent
import android.content.Intent.EXTRA_USER
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.dyv.dockyourvpnk.R
import com.example.dyv.dockyourvpnk.model.User
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val userConnected = intent.getSerializableExtra(EXTRA_USER) as? User

        activity_home_message.text = "Bonjour " + userConnected?.firstname + ", que souhaites tu faire ?"

        activity_home_vpn.setOnClickListener {
            val intent = Intent(this@HomeActivity, VpnActivity::class.java)
            intent.putExtra(EXTRA_USER,userConnected)
            startActivity(intent)
        }
    }
}
