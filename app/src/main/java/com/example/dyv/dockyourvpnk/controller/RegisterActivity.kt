package com.example.dyv.dockyourvpnk.controller

import android.content.Context
import android.content.Intent
import android.content.Intent.EXTRA_USER
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.dyv.dockyourvpnk.R
import com.example.dyv.dockyourvpnk.model.ApiConf
import com.example.dyv.dockyourvpnk.model.User
import com.example.dyv.dockyourvpnk.service.ApiService
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_register.*
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.Executors

class RegisterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        val apiConfUrl = ApiConf()
        val retrofit = Retrofit.Builder()
            .baseUrl(apiConfUrl.urlApi)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create<ApiService>(ApiService::class.java!!)
        val executor = Executors.newSingleThreadExecutor()

        activity_register_create_account.setOnClickListener {
            val username = activity_register_email.text.toString()
            val password = activity_register_password.text.toString()
            val lastname = activity_register_lastname.text.toString()
            val firstname = activity_register_firstname.text.toString()

            if(username.isNullOrEmpty() or password.isNullOrEmpty() or lastname.isNullOrEmpty() or firstname.isNullOrEmpty()){
                toast("Veuillez renseigner tout les champs")
            }else{
                executor.execute{
                    service.createUser(username, password, firstname, lastname).enqueue(object : retrofit2.Callback<User> {
                        override fun onFailure(call: Call<User>, t: Throwable) {
                            toast("Erreur réseau")
                        }
                        override fun onResponse(call: Call<User>, response: Response<User>){
                            if(response.body() is User){
                                toast("Création du compte Réussi, veuillez vous connectez")
                                val intent = Intent(this@RegisterActivity, HomeActivity::class.java)
                                startActivity(intent)
                            }else{
                                toast("Veuillez vérifier les informations renseignés")
                            }
                        }
                    })
                }
            }
        }
    }

    fun Context.toast(message: CharSequence) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}
