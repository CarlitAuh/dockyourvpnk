package com.example.dyv.dockyourvpnk.service

import com.example.dyv.dockyourvpnk.model.ApiMessageReponse
import retrofit2.Call
import retrofit2.http.*
import com.example.dyv.dockyourvpnk.model.User
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.POST
import retrofit2.http.FormUrlEncoded


interface ApiService {
    @FormUrlEncoded
    @POST("userConnexion")
    fun getCheckUserConnexion(@Field("username") first: String, @Field("password") last: String) :  Call<User>

    @FormUrlEncoded
    @POST("createUser")
    fun createUser(@Field("username") username: String, @Field("password") password: String, @Field("firstname") firstname: String, @Field("lastname") lastname: String) : Call<User>

    @GET("listUser")
    fun getUserList(): Call<List<User>>

    @FormUrlEncoded
    @POST("createSharedVolume")
    fun createSharedVolume(@Field("idClient") idClient: String, @Field("instanceId") instanceId : String) : Call<ResponseBody>

    @FormUrlEncoded
    @POST("genConfig")
    fun genConfig(@Field("idClient") idClient: String, @Field("instanceId") instanceId : String) : Call<ResponseBody>

    @FormUrlEncoded
    @POST("initPki")
    fun initPki(@Field("idClient") idClient: String, @Field("instanceId") instanceId : String) : Call<ResponseBody>

    @FormUrlEncoded
    @POST("genClient")
    fun genClient(@Field("idClient") idClient: String, @Field("instanceId") instanceId : String, @Field("pseudo") pseudo : String) : Call<ResponseBody>

    @FormUrlEncoded
    @POST("getClient")
    fun getClient(@Field("idClient") idClient: String, @Field("instanceId") instanceId : String, @Field("pseudo") pseudo : String) : Call<ApiMessageReponse>

    @FormUrlEncoded
    @POST("startOvpnServer")
    fun startOvpnServer(@Field("idClient") idClient: String, @Field("instanceId") instanceId : String) : Call<ResponseBody>
}