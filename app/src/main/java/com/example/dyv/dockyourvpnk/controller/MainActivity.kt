package com.example.dyv.dockyourvpnk.controller

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.dyv.dockyourvpnk.R
import com.example.dyv.dockyourvpnk.model.ApiConf
import com.example.dyv.dockyourvpnk.model.User
import com.example.dyv.dockyourvpnk.service.ApiService
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.Executors

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val apiConfUrl = ApiConf()
        val retrofit = Retrofit.Builder()
            .baseUrl(apiConfUrl.urlApi)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create<ApiService>(ApiService::class.java!!)
        val executor = Executors.newSingleThreadExecutor()

        activity_main_button_login.setOnClickListener {

            val username = activity_main_pseudo_login.text.toString()
            val password = activity_main_password_login.text.toString()

            if(username.isNullOrEmpty() or password.isNullOrEmpty()){
                toast("Veuillez renseigner votre Mail et votre Mot de passe")
            }else{
                executor.execute{
                    service.getCheckUserConnexion(username, password).enqueue(object : retrofit2.Callback<User> {
                        override fun onFailure(call: Call<User>, t: Throwable) {
                            toast("Erreur réseau")
                        }
                        override fun onResponse(call: Call<User>, response: Response<User>){
                            if(response.body() is User){
                                toast("Connexion reussie")
                                val intent = Intent(this@MainActivity, HomeActivity::class.java)
                                intent.putExtra(Intent.EXTRA_USER, response.body())
                                startActivity(intent)
                            }else{
                                toast("Erreur de Connexion, veuillez vérifier les identifiants")
                            }
                        }
                    })
                }
            }
        }

        activity_main_account_create.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }


        /*executor.execute{
            service.getUserList().enqueue(object : retrofit2.Callback<List<User>> {
                override fun onFailure(call: Call<List<User>>, t: Throwable) {
                    activity_main_test.text = "Erreur réseau"
                }

                override fun onResponse(call: Call<List<User>>, response: Response<List<User>>){
                    val userConnected = User()
                    userConnected.firstname = response.body()?.get(0)!!.firstname
                    activity_main_test.text = userConnected.firstname
                }
            })
        }*/
        }
    fun Context.toast(message: CharSequence) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}

