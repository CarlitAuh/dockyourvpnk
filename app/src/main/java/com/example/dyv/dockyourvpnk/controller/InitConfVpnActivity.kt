package com.example.dyv.dockyourvpnk.controller

import android.Manifest
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.dyv.dockyourvpnk.R
import com.example.dyv.dockyourvpnk.model.ApiConf
import com.example.dyv.dockyourvpnk.model.User
import com.example.dyv.dockyourvpnk.service.ApiService
import kotlinx.android.synthetic.main.activity_init_conf_vpn.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.Executors
import android.text.method.TextKeyListener.clear
import java.io.*
import android.media.MediaScannerConnection
import android.os.Environment
import java.nio.file.Files.exists
import android.R.attr.data
import android.content.pm.PackageManager
import android.support.v4.content.ContextCompat
import com.example.dyv.dockyourvpnk.model.ApiMessageReponse
import com.google.gson.Gson
import com.google.gson.JsonParser
import retrofit2.HttpException
import java.io.File.separator

class InitConfVpnActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_init_conf_vpn)

        val userConnected = intent.getSerializableExtra(Intent.EXTRA_USER) as? User

        val apiConfUrl = ApiConf()
        val retrofit = Retrofit.Builder()
            .baseUrl(apiConfUrl.urlApi)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create<ApiService>(ApiService::class.java!!)
        val executor = Executors.newSingleThreadExecutor()

        val idClient = userConnected?.id.toString()
        val instanceId = "15"
        val username = userConnected?.firstname.toString() + "-" + userConnected?.lastname.toString() + "-" + instanceId

        activity_initConfVpn_sharedVolume.setOnClickListener {
            executor.execute{
                service.createSharedVolume(idClient, instanceId).enqueue(object : retrofit2.Callback<ResponseBody> {
                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        toast("Erreur réseau")
                    }
                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>){
                        toast(response.code().toString())
                        if(response.code() == 200 || response.code() == 201){
                            toast("Creation du Volume en cours")
                        }else{
                            toast("Erreur dans la réponse")
                        }
                    }
                })
            }
        }

        activity_initConfVpn_genConf.setOnClickListener {
            executor.execute{
                service.genConfig(idClient, instanceId).enqueue(object : retrofit2.Callback<ResponseBody> {
                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        toast("Erreur réseau")
                    }
                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>){
                        toast(response.code().toString())
                        if(response.code() == 200 || response.code() == 201){
                            toast("Generation de la conf en cours")
                            activity_initConfVpn_getClient_message.text = response.message()
                        }else{
                            toast("Erreur dans la réponse")
                        }
                    }
                })
            }
        }

        activity_initConfVpn_initPki.setOnClickListener {
            executor.execute{
                service.initPki(idClient, instanceId).enqueue(object : retrofit2.Callback<ResponseBody> {
                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        toast("Erreur réseau")
                    }
                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>){
                        toast(response.code().toString())
                        if(response.code() == 200 || response.code() == 201){
                            toast("Generation de clé privée en cours")
                        }else{
                            toast("Erreur dans la réponse")
                        }
                    }
                })
            }
        }

        activity_initConfVpn_genClient.setOnClickListener {
            executor.execute{
                service.genClient(idClient, instanceId, username).enqueue(object : retrofit2.Callback<ResponseBody> {
                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        toast("Erreur réseau")
                    }
                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>){
                        toast(response.code().toString())
                        if(response.code() == 200 || response.code() == 201){
                            toast("Generation du Client en cours")
                        }else{
                            toast("Erreur dans la réponse")
                        }
                    }
                })
            }
        }
        activity_initConfVpn_getClient.setOnClickListener {
            executor.execute{
                service.getClient(idClient, instanceId, username).enqueue(object : retrofit2.Callback<ApiMessageReponse> {
                    override fun onFailure(call: Call<ApiMessageReponse>, t: Throwable) {
                        toast("Erreur réseau")
                    }
                    override fun onResponse(call: Call<ApiMessageReponse>, response: Response<ApiMessageReponse>){
                        toast(response.code().toString())
                        if(response.code() == 200 || response.code() == 201){
                            toast("Generation du ficher de CONF en cours")

                            val baseFolder: String?
                            if (Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED) {
                                baseFolder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString()
                            } else {
                                baseFolder = getFilesDir().getAbsolutePath()
                            }

                            val string = response.body()?.message
                            val file = File(baseFolder + File.separator + username + ".ovpn")
                            file.getParentFile().mkdirs()
                            val fos = FileOutputStream(file)
                            fos.write(string?.toByteArray())
                            fos.flush()
                            fos.close()

                            toast("OK Bitch")
                        }else{
                            toast("Erreur dans la réponse")
                        }
                    }
                })
            }
        }
        activity_initConfVpn_startOvpnSever.setOnClickListener {
            executor.execute{
                service.startOvpnServer(idClient, instanceId).enqueue(object : retrofit2.Callback<ResponseBody> {
                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        toast("Erreur réseau")
                    }
                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>){
                        toast(response.code().toString())
                        if(response.code() == 200 || response.code() == 201){
                            toast("Démarrage du VPN")
                        }else{
                            toast("Erreur dans la réponse")
                        }
                    }
                })
            }
        }
    }
    fun Context.toast(message: CharSequence) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}
