package com.example.dyv.dockyourvpnk.controller

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.dyv.dockyourvpnk.R
import com.example.dyv.dockyourvpnk.model.User
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_vpn.*
import android.support.v4.content.ContextCompat.startActivity


class VpnActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vpn)

        val userConnected = intent.getSerializableExtra(Intent.EXTRA_USER) as? User

        activity_vpn_message.text = "Bonjour " + userConnected?.firstname

        activity_vpn_init.setOnClickListener {
            val intent = Intent(this@VpnActivity, InitConfVpnActivity::class.java)
            intent.putExtra(Intent.EXTRA_USER,userConnected)
            startActivity(intent)
        }

        activity_vpn_run.setOnClickListener {
            startNewActivity(this,"net.openvpn.openvpn")
        }
    }

    fun startNewActivity(context: Context, packageName: String) {
        var intent = context.getPackageManager().getLaunchIntentForPackage(packageName)
        if (intent == null) {
            // Bring user to the market or let them choose an app?
            intent = Intent(Intent.ACTION_VIEW)
            intent!!.setData(Uri.parse("market://details?id=$packageName"))
        }
        intent!!.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(intent)
    }

}
