package com.example.dyv.dockyourvpnk.model

import java.io.Serializable

data class ApiConf(
    var urlApi: String = "http://34.76.227.97/api/") : Serializable


data class ApiMessageReponse(
    var message: String = "") : Serializable